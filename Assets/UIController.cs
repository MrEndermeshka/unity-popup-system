using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController Instance;
    public RectTransform MainCanvas;
    // Start is called before the first frame update
    void Start()
    {
        if (Instance != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        Instance = this;
    }

    public GameObject CreatePopup()
    {
        GameObject popup = Instantiate(Resources.Load("Popup") as GameObject, Vector3.zero, Quaternion.identity);
        return popup;
    }


}
