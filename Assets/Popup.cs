using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public class Popup : MonoBehaviour
{

    [SerializeField] Button _button1;
    [SerializeField] Button _button2;
    [SerializeField] Button _button3;
    [SerializeField] Text _button1Text;
    [SerializeField] Text _button2Text;
    [SerializeField] Text _popupText;
    [SerializeField] RawImage _popupImage;

    // Start is called before the first frame update

    public void Init(RectTransform canvas, string popupMessage, string popupImageUrl, string btn1txt, string btn2txt, List<string> btnUrl, Action action)
    {
        _popupText.text = popupMessage;
        _button1Text.text = btn1txt;
        _button2Text.text = btn2txt;
        StartCoroutine(LoadImage(popupImageUrl, _popupImage));
        var x = new List<Button>();
        x.Add(_button1);
        x.Add(_button2);
        int i = 0;
        foreach (var button in btnUrl)
        {
            StartCoroutine(LoadSprites(button, x[i]));
            i++;
        }

        transform.SetParent(canvas);
        transform.SetAsFirstSibling();
        transform.localScale = new Vector3(0.5f,0.5f,0.5f);
        transform.localPosition = Vector3.zero;
        _button1.onClick.AddListener(() =>
        {
            GameObject.Destroy(gameObject);
        });
        _button2.onClick.AddListener(() =>
        {
            action();
        });
    }
    private IEnumerator LoadSprites(string url, Button btn)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D textureFromUrl = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Sprite newSprite = Sprite.Create(textureFromUrl, new Rect(0, 0, textureFromUrl.width, textureFromUrl.height), new Vector2(0.3f, 0.3f));

            btn.GetComponent<Image>().sprite = newSprite;

        }
    }
    private IEnumerator LoadImage(string url, RawImage popupImage)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D textureFromUrl = ((DownloadHandlerTexture)request.downloadHandler).texture;
            popupImage.texture = textureFromUrl;

        }
    }
    public void MakeTransition()
    {
        _button3.GetComponent<Animator>().SetTrigger("MakeAnimation");
    }
}
