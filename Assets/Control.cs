using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Control : MonoBehaviour
{
    [SerializeField] Button _test1;
    [SerializeField] Button _test2;
    // Start is called before the first frame update
    public GameObject currentPopup = null;
    public GameObject nextPopup = null;
    private Queue<GameObject> uiQueue = new Queue<GameObject>();
    void Start()
    {
        _test1.onClick.AddListener(() =>
        {
            int queueInOrder;
            Action action = functionTest1();
            currentPopup = UIController.Instance.CreatePopup();
            Popup popup = currentPopup.GetComponent<Popup>();
            uiQueue.Enqueue(currentPopup);
            List<string> str = new List<string>();
            str.Add("https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Cancel_icon.svg/1024px-Cancel_icon.svg.png");
            str.Add("https://cdn-icons-png.flaticon.com/512/87/87413.png");
            popup.Init(UIController.Instance.MainCanvas,
                    "Do you want to open site?",
                    "https://telegram.org/file/464001916/10d69/wMJtQWE_ZwI.17701.png/f4e97997cb38fc577a",
                    "Yes",
                    "No",
                    str,
                    action
                    );
        });
        _test2.onClick.AddListener(() =>
        {
            Action action = functionTest2();
            currentPopup = UIController.Instance.CreatePopup();
            Popup popup = currentPopup.GetComponent<Popup>();
            uiQueue.Enqueue(currentPopup);
            List<string> str = new List<string>();
            str.Add("https://www.counselmagazine.co.uk/images/uploadedfiles/2897df9f91d167a042c7a1058355e238561e.png?sfvrsn=c5df5f0a_3");
            str.Add("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Yes_._logo.svg/1200px-Yes_._logo.svg.png");
            popup.Init(UIController.Instance.MainCanvas,
                "Do you want to close the game?",
                "https://static.vecteezy.com/system/resources/previews/002/896/742/original/sad-boy-depressed-boy-looking-lonely-free-vector.jpg",
                "Yes",
                "Not realy",
                str,
                action
                );
        });
    }

    Action functionTest1()
    {
        Action action = () =>
        {
            Application.OpenURL("https://t.me/mrendermeshka");
        };
        return action;
    }
    Action functionTest2()
    {
        Action action = () =>
        {
            Application.Quit();
        };
        return action;
    }


}
